package cl.cmoscoso.uberCabify.enumeration;

public enum FileTypeEnum {
	CABIFY, UBER, AUTOPISTA_CENTRAL, COSTANERA_NORTE, VESPUCIO_NORTE, VESPUCIO_SUR, UNKNOW;
}
