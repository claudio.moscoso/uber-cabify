package cl.cmoscoso.uberCabify.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import cl.cmoscoso.uberCabify.entity.CobroEntity;
import cl.cmoscoso.uberCabify.entity.UberCabifyFileEntity;
import cl.cmoscoso.uberCabify.enumeration.FileTypeEnum;
import cl.cmoscoso.uberCabify.service.FileParserService;

@Component
public class MainProgram {
	private static Logger LOG = LoggerFactory.getLogger(MainProgram.class);
	private String[] firstLineFields = { "Patente", "FechaHora", "Portico", "Concesionaria", "TAG", "Horario",
			"Importe", "Factura" };

	@Autowired
	@Qualifier("CostaneraNorteParser")
	FileParserService costaneraNorteParser;

	@Autowired
	@Qualifier("AutopistaCentralParser")
	FileParserService autopistaCentralParser;

	@Autowired
	@Qualifier("VespucioNorteParser")
	FileParserService vespucioNorteParser;

	@Autowired
	@Qualifier("VespucioSurParser")
	FileParserService vespucioSurParser;

	public void start(String[] args) throws Exception {
		args = validateParameters(args);

		String path = args[0];
		String outputFile = args[1];

		String[] fileNames = readFilesList(path);
		List<UberCabifyFileEntity> files = clasification(fileNames);

		processFiles(path, files, path, outputFile);

	}

	private void processFiles(String path, List<UberCabifyFileEntity> files, String pathFiles, String outputFile) {
		String fullPathFileName = getFullPathFileName(pathFiles, outputFile);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = intiOutputfile(workbook, "Consolidado de Gastos");

		processFileList(path, files, sheet);

		writeFile(fullPathFileName, workbook);
	}

	private void processFileList(String path, List<UberCabifyFileEntity> files, XSSFSheet sheet) {
		for (UberCabifyFileEntity file : files) {
			if (file.getFileType().equals(FileTypeEnum.UNKNOW)) {
				LOG.warn("Archivo '{}' es desconocido, no será procesado...", file.getFileName());
			} else {
				LOG.debug("A procesar el archivo '{}'", file.getFileName());

				FileParserService fileParser = getFileParser(file.getFileType());
				List<CobroEntity> fileContent = getFileContent(fileParser, path, file.getFileName());

				swapToSheet(fileContent, sheet);
			}
		}
	}

	private void swapToSheet(List<CobroEntity> fileContent, XSSFSheet sheet) {
		XSSFRow row = null;
		XSSFCell cell = null;

		XSSFWorkbook workbook = sheet.getWorkbook();
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy HH:mm:ss"));

		for (CobroEntity cobro : fileContent) {
			row = sheet.createRow(sheet.getLastRowNum() + 1);
			row.createCell(0).setCellValue(cobro.getPatente());

			cell = row.createCell(1);
			cell.setCellValue(localDateTimeToDate(cobro.getFechaHora()));
			cell.setCellStyle(cellStyle);

//			row.createCell(1).setCellValue(formatoFecha(localDateTimeToDate(cobro.getFechaHora())));

			row.createCell(2).setCellValue(cobro.getPortico());
			row.createCell(3).setCellValue(cobro.getConcesionaria());
			row.createCell(4).setCellValue("");
			row.createCell(5).setCellValue(cobro.getHorario());
			row.createCell(6).setCellValue(cobro.getImporte().doubleValue());
			row.createCell(7).setCellValue(cobro.getFactura());

		}

	}

//	private Calendar formatoFecha(Calendar localDateTimeToDate) {
//		CellStyle cellStyle = wb.createCellStyle();
//		CreationHelper createHelper = wb.getCreationHelper();
//		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
//
//		cell = row.createCell(1);
//		cell.setCellValue(new Date());
//		cell.setCellStyle(cellStyle);
//
//	}

	public Calendar localDateTimeToDate(LocalDateTime localDateTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(localDateTime.getYear(), localDateTime.getMonthValue() - 1, localDateTime.getDayOfMonth(),
				localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
		return calendar;
	}

	private List<CobroEntity> getFileContent(FileParserService fileParser, String path, String fileName) {
//		fileParser.setFileName(getFullPathFileName(path, fileName));
		List<CobroEntity> cobros = null;
		String pathFileName = getFullPathFileName(path, fileName);
		try {
			cobros = fileParser.listCobro(pathFileName);
		} catch (FileNotFoundException e) {
			LOG.warn(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return cobros;
	}

	private FileParserService getFileParser(FileTypeEnum fileType) {
		FileParserService out = null;
		switch (fileType) {
		case AUTOPISTA_CENTRAL:
			out = autopistaCentralParser;
			break;
		case COSTANERA_NORTE:
			out = costaneraNorteParser;
			break;
		case CABIFY:

			break;
		case UBER:

			break;
		case VESPUCIO_NORTE:
			out = vespucioNorteParser;
			break;
		case VESPUCIO_SUR:
			out = vespucioSurParser;
			break;
		default:
			break;
		}

		return out;
	}

	private XSSFSheet intiOutputfile(XSSFWorkbook workbook, String sheetName) {
//		workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName);

		buildFirstLine(sheet);

		return sheet;
//		Object[][] datatypes = { { "Datatype", "Type", "Size(in bytes)" }, { "int", "Primitive", 2 },
//				{ "float", "Primitive", 4 }, { "double", "Primitive", 8 }, { "char", "Primitive", 1 },
//				{ "String", "Non-Primitive", "No fixed size" } };
//
//		int rowNum = 0;
//		LOG.debug("Creating excel");
//
//		for (Object[] datatype : datatypes) {
//			Row row = sheet.createRow(rowNum++);
//			int colNum = 0;
//			for (Object field : datatype) {
//				Cell cell = row.createCell(colNum++);
//				if (field instanceof String) {
//					cell.setCellValue((String) field);
//				} else if (field instanceof Integer) {
//					cell.setCellValue((Integer) field);
//				}
//			}
//		}

	}

	private void writeFile(String fullPathFileName, XSSFWorkbook workbook) {
		try {
			FileOutputStream outputStream = new FileOutputStream(fullPathFileName);
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private void buildFirstLine(XSSFSheet sheet) {
		int i = 0;
		Row row = sheet.createRow(0);
		for (String cellContent : firstLineFields) {
			Cell cell = row.createCell(i++);
			cell.setCellValue(cellContent);
		}
	}

	private String getFullPathFileName(String pathFiles, String outputFile) {
		String out = pathFiles;
		out = out.concat(File.separator);
		out = out.concat(outputFile);
		return out;
	}

	private List<UberCabifyFileEntity> clasification(String[] fileNames) {
		List<UberCabifyFileEntity> out = new ArrayList<UberCabifyFileEntity>(fileNames.length);
		FileTypeEnum type = null;

		for (String fileName : fileNames) {
			fileName = fileName.toLowerCase();

			if (fileName.indexOf("autopista") > -1 && fileName.indexOf("central") > -1) {
				type = FileTypeEnum.AUTOPISTA_CENTRAL;
			} else {
				if (fileName.indexOf("cabify") > -1) {
					type = FileTypeEnum.CABIFY;
				} else {
					if (fileName.indexOf("uber") > -1) {
						type = FileTypeEnum.UBER;
					} else {
						if (fileName.indexOf("costanera") > -1 && fileName.indexOf("norte") > -1) {
							type = FileTypeEnum.COSTANERA_NORTE;
						} else {
							if (fileName.indexOf("vespucio") > -1 && fileName.indexOf("norte") > -1) {
								type = FileTypeEnum.VESPUCIO_NORTE;
							} else {
								if (fileName.indexOf("vespucio") > -1 && fileName.indexOf("sur") > -1) {
									type = FileTypeEnum.VESPUCIO_SUR;
								} else {
									type = FileTypeEnum.UNKNOW;
								}
							}
						}
					}
				}
			}
			out.add(new UberCabifyFileEntity(fileName, type));
		}
		return out;
	}

	private String[] readFilesList(String pathFiles) {
		String[] out = new String[0];
//		String[] temp = new String[0];

		File folder = new File(pathFiles);

		if (folder.isDirectory()) {
			int i = 0;
			File[] files = folder.listFiles();
			for (File file : files) {
				if (file.isFile()) {
					out = Arrays.copyOf(out, i + 1);
					out[i++] = file.getName();
				}
			}
		}

		return out;
	}

	private String[] validateParameters(String[] args) {
		List<String> tmp = new ArrayList<String>();
		for (String arg : args) {
			if (!arg.startsWith("--spring")) {
				tmp.add(arg);
			}
		}

		String arr[] = new String[tmp.size()];
		args = tmp.toArray(arr);

		if (args.length == 0) {
			String msg = "No se especificaron parametros.\n";
			msg += "Se requiere especificar carpeta y nombre-archivo-salida.";
			throw new RuntimeException(msg);
		}
		return args;
	}

}
