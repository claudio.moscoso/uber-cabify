package cl.cmoscoso.uberCabify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cl.cmoscoso.uberCabify.controller.MainProgram;

@SpringBootApplication
public class UberCabifyApplication implements CommandLineRunner {
	private static Logger LOG = LoggerFactory.getLogger(UberCabifyApplication.class);

	@Autowired
	MainProgram main;

	public static void main(String[] args) {

		SpringApplication.run(UberCabifyApplication.class, args);
	}

	@Override
	public void run(String... args) {
		LOG.debug("Parametros:");
		for (String arg : args) {
			LOG.debug("arg='{}'", arg);
		}
		try {
			LOG.debug("Start");
			main.start(args);
			LOG.debug("Done");
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
}
