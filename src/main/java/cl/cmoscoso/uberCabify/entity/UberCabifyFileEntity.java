package cl.cmoscoso.uberCabify.entity;

import cl.cmoscoso.uberCabify.enumeration.FileTypeEnum;

public class UberCabifyFileEntity {
	private String fileName;
	private FileTypeEnum fileType;

	public UberCabifyFileEntity() {
	}

	public UberCabifyFileEntity(String fileName, FileTypeEnum fileType) {
		super();
		this.fileName = fileName;
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public FileTypeEnum getFileType() {
		return fileType;
	}

	public void setFileType(FileTypeEnum fileType) {
		this.fileType = fileType;
	}

	@Override
	public String toString() {
		return "UberCabifyFileEntity [fileName=" + fileName + ", fileType=" + fileType + "]";
	}

}
