package cl.cmoscoso.uberCabify.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CobroEntity {
	String patente = null;
	LocalDateTime fechaHora = null;
	String portico = null;
	String concesionaria = null;
	String horario = null;
	BigDecimal importe = null;
	String factura = null;

	public CobroEntity() {
	}

	public CobroEntity(String patente, LocalDateTime fechaHora, String portico, String concesionaria, String horario,
			BigDecimal importe, String factura) {
		super();
		this.patente = patente;
		this.fechaHora = fechaHora;
		this.portico = portico;
		this.concesionaria = concesionaria;
		this.horario = horario;
		this.importe = importe;
		this.factura = factura;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getPortico() {
		return portico;
	}

	public void setPortico(String portico) {
		this.portico = portico;
	}

	public String getConcesionaria() {
		return concesionaria;
	}

	public void setConcesionaria(String concesionaria) {
		this.concesionaria = concesionaria;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getFactura() {
		return factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	@Override
	public String toString() {
		return "CobroEntity [patente=" + patente + ", fechaHora=" + fechaHora + ", portico=" + portico
				+ ", concesionaria=" + concesionaria + ", horario=" + horario + ", importe=" + importe + ", factura="
				+ factura + "]";
	}
}
