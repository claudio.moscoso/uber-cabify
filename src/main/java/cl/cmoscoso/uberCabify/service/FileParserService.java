package cl.cmoscoso.uberCabify.service;

import java.io.FileNotFoundException;
import java.util.List;

import cl.cmoscoso.uberCabify.entity.CobroEntity;

public interface FileParserService {
	public List<CobroEntity> listCobro(String file) throws FileNotFoundException, Exception;

}
