package cl.cmoscoso.uberCabify.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import cl.cmoscoso.uberCabify.service.FileParserService;

@Component("AutopistaCentralParser")
public class AutopistaCentralParserImpl extends AbstractCSVParserImpl implements FileParserService {
	private static Logger LOG = LoggerFactory.getLogger(AutopistaCentralParserImpl.class);

	public String getConcesionaria(String[] line) {
		return "AutopistaCentral";
	}

	public String getFactura(String[] line) {
		return line[2];
	}

	public LocalDateTime getFechaHora(String[] line) {
		return convertToDateTime(line[7], " "+line[8]);
	}

	public String getHorario(String[] line) {
		return line[9];
	}

	public BigDecimal getImporte(String[] line) {
		return toBigdecimal(line[10]);
	}

	public String getPatente(String[] line) {
		return line[3];
	}

	public String getPortico(String[] line) {
		return line[4];
	}

}
