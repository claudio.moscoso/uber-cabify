package cl.cmoscoso.uberCabify.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import cl.cmoscoso.uberCabify.service.FileParserService;

@Component("CostaneraNorteParser")
public class CostaneraNorteParserImpl extends AbstractCSVParserImpl implements FileParserService {
	/**
	 * <code>
	FechaHora
	Portico
	Patente
	Categoria
	Tag
	Kms
	Importe
	Comprobante
	Eje
	</code>
	 */
	@Override
	String getConcesionaria(String[] line) {
		return "CostaneraNorte";
	}

	@Override
	String getFactura(String[] line) {
		return line[7];
	}

	@Override
	LocalDateTime getFechaHora(String[] line) {
		return convertToDateTime("", line[0], "dd/MM/yyyy HH:mm:ss");
	}

	@Override
	String getHorario(String[] line) {
		return line[3] + "?";
	}

	@Override
	BigDecimal getImporte(String[] line) {
		return toBigdecimal(line[6]);
	}

	@Override
	String getPatente(String[] line) {
		return line[2];
	}

	@Override
	String getPortico(String[] line) {
		return line[1];
	}

}
