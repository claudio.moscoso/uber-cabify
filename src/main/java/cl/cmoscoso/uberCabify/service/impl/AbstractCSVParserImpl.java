package cl.cmoscoso.uberCabify.service.impl;

import java.io.FileReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;

import cl.cmoscoso.uberCabify.entity.CobroEntity;

public abstract class AbstractCSVParserImpl {
	private static Logger LOG = LoggerFactory.getLogger(AbstractCSVParserImpl.class);

	abstract String getConcesionaria(String[] line);

	abstract String getFactura(String[] line);

	abstract LocalDateTime getFechaHora(String[] line);

	abstract String getHorario(String[] line);

	abstract BigDecimal getImporte(String[] line);

	abstract String getPatente(String[] line);

	abstract String getPortico(String[] line);

	protected LocalDateTime convertToDateTime(String date, String time) {
		return convertToDateTime(date, time, "yyyy-MM-dd HH:mm:ss");
	}

	protected LocalDateTime convertToDateTime(String date, String time, String pattern) {
		String dateTime = String.format("%s%s".trim(), date, time);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime out = LocalDateTime.parse(dateTime, formatter);
		return out;
	}

	public List<CobroEntity> listCobro(String fileName) throws Exception {
		List<CobroEntity> out = new ArrayList<CobroEntity>();
		boolean isFirst = true;
		CobroEntity cobro = null;

//		String fileName = getFileName();
		FileReader fileReader = new FileReader(fileName);
		CSVReader reader = new CSVReader(fileReader, ';');

		String[] line;
		while ((line = reader.readNext()) != null) {
			if (!isFirst) {
				cobro = new CobroEntity();

				cobro.setConcesionaria(getConcesionaria(line));
				cobro.setFactura(getFactura(line));
				cobro.setFechaHora(getFechaHora(line));
				cobro.setHorario(getHorario(line));
				cobro.setImporte(getImporte(line));
				cobro.setPatente(getPatente(line));
				cobro.setPortico(getPortico(line));

				out.add(cobro);
				LOG.debug(cobro.toString());
			} else {
				String headers = "";
				for (String headName : line) {
					headers = headers.concat(headName).concat("\t");
				}
				LOG.debug(headers);
				isFirst = false;
			}
//			Thread.sleep(2000);
		}
		reader.close();
		fileReader.close();
		return out;

	}

	protected BigDecimal toBigdecimal(String value) {
		value = value.replaceAll("[,]", ".");
		return new BigDecimal(value);
	}
}
