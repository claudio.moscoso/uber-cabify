package cl.cmoscoso.uberCabify.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import cl.cmoscoso.uberCabify.entity.CobroEntity;
import cl.cmoscoso.uberCabify.service.FileParserService;

@Component("VespucioNorteParser")
public class VespucioNorteParserImpl extends AbstractCSVParserImpl implements FileParserService {
	private static Logger LOG = LoggerFactory.getLogger(VespucioNorteParserImpl.class);

	@Override
	public List<CobroEntity> listCobro(String fileName) throws FileNotFoundException, Exception {
		LOG.debug("----------------->" + fileName);
		boolean isFirst = true;

		List<CobroEntity> out = new ArrayList<CobroEntity>();
		CobroEntity cobro = null;

		String[] line = new String[10];

		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);

		// Finds the workbook instance for XLSX file
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		// Return first sheet from the XLSX workbook
		XSSFSheet sheet = workbook.getSheetAt(0);

//		int i = 0;
		XSSFRow row = null; // sheet.getRow(i);
		XSSFCell cell = null;

		int n = sheet.getLastRowNum();
		int m = 0;
		LOG.debug("N=" + n);
		for (int i = 0; i <= n; i++) {
			row = sheet.getRow(i);
//			LOG.debug("" + i + " -----> " + row.toString().substring(0, 100));

			if (isFirst) {
				isFirst = false;
			} else {
				m = row.getLastCellNum();
//				LOG.debug("cells -> " + m);
				for (int j = 0; j <= m; j++) {
					cell = row.getCell(j);
					if (cell != null) {
						line[j] = cell.toString();
					}
//					if (LOG.isDebugEnabled()) {
//						LOG.debug("cell ({}) {}", j, cell == null ? "NULL" : line[j]);
//					}
				}

//				if (LOG.isDebugEnabled()) {
//					LOG.debug("Line={}", line.toString());
//				}

				cobro = new CobroEntity();
				cobro.setConcesionaria("VespucioNorte");
				cobro.setFactura(getFactura(line));
				cobro.setFechaHora(getFechaHora(line));
				cobro.setHorario(getHorario(line));
				cobro.setImporte(getImporte(line));
				cobro.setPatente(getPatente(line));
				cobro.setPortico(getPortico(line));

				out.add(cobro);

			}

		}

		workbook.close();
		fis.close();

		return out;
	}

	@Override
	String getConcesionaria(String[] line) {
		return "Vespucio Norte " + line[6];
	}

	@Override
	String getFactura(String[] line) {
		return "?";
	}

	@Override
	LocalDateTime getFechaHora(String[] line) {
		return convertToDateTime(line[1], " " + line[2], "dd-MM-yyyy HH:mm:ss");
	}

	@Override
	String getHorario(String[] line) {
		String out = "";
		switch (line[7]) {
		case "TBP":
			out = "Alta";
			break;
		case "TBFP":
			out = "Normal";
			break;
		default:
			out = line[7];
			break;
		}
		return out;
	}

	@Override
	BigDecimal getImporte(String[] line) {
		return new BigDecimal(line[8]);
	}

	@Override
	String getPatente(String[] line) {
		return line[0];
	}

	@Override
	String getPortico(String[] line) {
		return line[4];
	}

}
